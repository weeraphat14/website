from flask import Flask, render_template, redirect, url_for, request
import calculator as cal

app = Flask(__name__)

@app.route('/calculate', methods=['GET', 'POST'])
def calculate():
    result = None
    if request.method == 'POST':
        if request.form['value1'] != '' and request.form['value2'] != '':
            if request.form['sign'] == "1":
                result = cal.plus(request.form['value1'],request.form['value2'])
            elif request.form['sign'] == "2":
                result = cal.minus(request.form['value1'],request.form['value2'])
            elif request.form['sign'] == "3":
                result = cal.mul(request.form['value1'],request.form['value2'])
            elif request.form['sign'] == "4":
                result = cal.div(request.form['value1'],request.form['value2'])
        else:
            return redirect(url_for('home'))
    return render_template('calculate.html', result=result)

if __name__ == '__main__':
  app.debug = True
  app.run(host='0.0.0.0', port=8000)