import unittest

from website import calculator as cal


class calculatorTest(unittest.TestCase):
    def test_plus_data(self):
        answer = cal.plus(5,10)
        expected = 15
        self.assertEqual(expected, answer)

    def test_minus_data(self):
        answer = cal.minus(20,10)
        expected = 10
        self.assertEqual(expected, answer)

    def test_multiply_data(self):
        answer = cal.mul(50,2)
        expected = 100
        self.assertEqual(expected, answer)

    def test_divide_data(self):
        answer = cal.div(100,10)
        expected = 10
        self.assertEqual(expected, answer)
    
    def test_plus_is_not_string(self):
        answerplus = cal.plus(5,10)
        self.assertIsInstance(answerplus,float)

    def test_minus_is_not_string(self):
        answerminus = cal.minus(20,10)
        self.assertIsInstance(answerminus,float)

    def test_multiply_is_not_string(self):
        answermultiply = cal.mul(50,2)
        self.assertIsInstance(answermultiply,float)

    def test_divide_is_not_string(self):
        answerdivide = cal.div(100,10)
        self.assertIsInstance(answerdivide,float)

if __name__ == '__main__':
    unittest.main()
